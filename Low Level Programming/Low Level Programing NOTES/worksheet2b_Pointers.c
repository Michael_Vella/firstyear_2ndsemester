#include <stdio.h>
int main(void)
{

	/*1.  Declare the following pointers
	(a) 	iPtr pointer to integer
	(b) 	cPtr pointer to character
	(c) 	fPtr pointer to float
	(d) ipp pointer to pointer to integer */

	int *iPtr;
	char *cPtr;
	float *fPtr;
	int **ipp;

	/*
	2.	Write a program to do the following :
		(a)Declare a pointer to integer, called ip and set it to NULL.
		(b)Declare an integer called number and set a value in it.
		(c)Set your pointer to point to the variable number.
		(d)Display the contents of the pointer ip.What does this value represent ?
		(e)Display the value of the variable to which the pointer ip is pointing(use only ip 	variable).
		(f)Display the address of the pointer variable, ip.
		(g)Change the value of the variable by using only the pointer ip.
	*/
	int *ip = NULL;
	int number = 10;
	ip = &number;
	printf("\nQuestion 2\n");
	printf("The contents of the pointer is %p\n ", ip); //address of number
	printf("The value of the variable pointed to by  pointer is %d\n ", *ip);
	printf("The address of the pointer is %x \n ", &ip);
	*ip = 5;
	printf("The value of the variable is now %d \n", number);

	/*
	3.	Write a program to do the following:
	(a) 	Declare two pointers to character, cPtr1 and cPtr2.
	(b)	Declare a character variable ch and store the value �A�;
	(c)	Set the pointer cPtr1 to point to ch.
	(d)	Set cPtr2 to store the contents of cPtr1?
	(e) Write code to show that the two pointers are now pointing to the same variable.
	*/

	char *cPtr1;
	char *cPtr2;
	char ch = 'A';
	cPtr1 = &ch;
	cPtr2 = cPtr1;
	printf("\nQuestion 3\n");
	printf("The value of cell pointed to by cPtr1 is %c\n", *cPtr1);
	printf("The value of cell pointed to by cPtr2 is %c\n", *cPtr2);





	/*
	4.	Write a program to do the following:
	(a)	Set a pointer to point to a integer variable;
	(b)	Display the contents of the pointer.
	(c)	Increment the pointer by 1.  What happens?  Why?
	(d)	Can you display the contents of the memory cell to which the pointer is now pointing?
	(d)	Can you change the contents of the memory cell to which the pointer is now pointing?

	*/
	printf("\nQuestion 4\n");
	int * ip2 = NULL;
	int num;
	ip2 = &num;
	printf("Address stored in ip is %p\n", ip2);
	ip2 += 1;
	printf("Address stored in ip is %p\n", ip2);
	printf("Value stored in cell pointed to by IP is %d\n", *ip2);
	*ip2 = 15;
	printf("Value stored in cell pointed to by IP is %d\n", *ip2); //works


	/*
	5.	Write a program to do the following:
	(a)	Declare a pointer to integer numPtr.
	(b)	Set a value to the address pointed to by numPtr.  What happens?  Why?
	(c)	Can we store a memory address in the pointer variable directly?
	(d)	What happens if you try to change the contents of this memory address? Why?

	*/
	printf("\nQuestion 5\n");
	int *numPtr;
	
	//*numPtr = 5; // runtime error using pointer without initialising it
	numPtr = 0x1234;
	//*numPtr = 15; //access violation

	/*
	6.	Write a program to do the following:
	(a)	Declare a pointer to integer.
	(b)	Allocate memory to the pointer by using malloc().
	(c)	Store a value in the memory pointed to by your pointer.
	(d)	Write code to display the contents of the memory pointed to by your pointer.
	(e)  Free memory allocated to the pointer by using free().

	*/
	printf("\nQuestion 6\n");
	int * ipointer;
	ipointer = (int)malloc(sizeof(int));
	*ipointer = 10;
	printf("The address stored in pointer is %p\n", ipointer);
	printf("The value of cell pointed to by pointer is %d\n ", *ipointer);
	free(ipointer);
	printf("The address stored in pointer is %p\n ", ipointer);
	printf("The value of cell pointed to by pointer is now %d\n ", *ipointer);


	getch();
}