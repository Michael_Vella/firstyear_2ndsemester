#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define N 100 //number of elements in array

int main(void)
{
	int numArray[N]; //array of 100 integers
	srand(time(NULL)); // Intializes random number generator
	//Store some random numbers in the array
	for (int i = 0; i < N; i++)
		numArray[i] = rand() % 200; //generate random number

	//display the numbers in unsorted order
	for (int i = 0; i < N; i++)
		printf("%10d", numArray[i]); 

	//sorting using insertion sort
	int unsorted; //first index of unsorted region
	int loc; //index of insertion in sorted region
	int nextUnsortedItem; //next item in unsorted region
	bool done; //note inclusion of stdbool.h header file
	for (unsorted = 1; unsorted < N; unsorted++)
	{
		nextUnsortedItem = numArray[unsorted];
		loc = unsorted;
		done = false;
		while (loc > 0 && !done)
		{
			if (numArray[loc - 1] > nextUnsortedItem)
			{
				numArray[loc] = numArray[loc - 1]; //shift right
				loc = loc - 1;
			}
			else
				done = true;
		}
		//insert NextUnsortedItem into sorted region
		numArray[loc] = nextUnsortedItem;
	} //end sorting
	
	//display the numbers in unsorted order
	printf("\nSorting...");
	for (int i = 0; i < N; i++)
		printf("%10d", numArray[i]);

	//search the numbers using a sorted sequential search algorithm
	printf("\nEnter item to be searched ");
	int key;
	scanf("%d", &key);
	bool found = false;
	int pos = 0;
	while (numArray[pos] <= key && !found && pos < N)
	{
		if (numArray[pos] == key)
			found = true;
		else
			pos++;
	}
	if (found)
		printf("\nitem found at position: %d ", pos);
	else
		printf("\nitem not found");

	getch();
}