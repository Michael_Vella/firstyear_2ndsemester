#include <stdio.h>
int main(void)
{

	int i = 5;
	int j = 5;

	printf("The value of i is %d.  The value of j is %d", i++, ++j); // output:  i is 5, j is 6
	printf("\nThe value of i is %d.  The value of j is %d", i, j); //output: i is 6, j is 6
	/*
		Remember that 
		i++ means --> use the value of i first then increment it after the  WHOLE statement is executed
		++i means --> first increment the value of i then use it.
	*/

	//What is the value of the k variable at the end of the following snippet ?
	
	int k;
	i = 4;
	j = 5;
	k = --i * j++;  // k is 15
	// k = 3 * 5 = 15
	// after executing j is 6;
	
	int a = k; // a is 15

	//What is the value of k?
	i = 4;
	j = 5;
	k = i-- * ++j;
	// k = 4 * 6 = 24
	// i becomes 3
	getch();

	//what is the value of k?
	i = 2;
	j = -2;
	if (i) //i is 2 
		i--;// i is 1 -- this will be executed because i is NOT ZERO
	if (j) //j is -2
		j++;//j is -1 -- this will be executed because j is NOT ZERO
	k = i * j; // 1 * -1 = -1
	printf("%d", k); //-1
	getch();
}