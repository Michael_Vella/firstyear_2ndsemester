#include <stdio.h>

int main(void)
{
	int x = 1;
	//example 1
	if (x==1)
		printf("\nTRUE PART: Value of x is %d", x); //this part is executed
	else
		printf("\nFALSE PART: Value of x is %d", x);
	
	//example 2
	if (x)
		printf("\nTRUE PART: Value of x is %d", x); //x is considered as true here because it is 1
	else
		printf("\nFALSE PART: Value of x is %d", x);

	//example 3
	x = 5;
	if (x)  //if value of x is NOT ZERO (i.e. not false)
		printf("\nTRUE PART: Value of x is %d", x);  //this is executed
	else  //if value of x is ZERO (ie is FALSE)
		printf("\nFALSE PART: Value of x is %d", x);

	//example 4
	if (x=6)  //be careful!  This is an assignment stmt and it will still compile without errors
		printf("\nTRUE PART: Value of x is %d", x);  //this is executed
	else
		printf("\nFALSE PART: Value of x is %d", x);

	//example 5
	x = -5;
	if (x)
		printf("\nTRUE PART: Value of x is %d", x); //this is executed
	else
		printf("\nFALSE PART: Value of x is %d", x);

	//example 6
	x = 0;
	if (x)
		printf("\nTRUE PART: Value of x is %d", x); 
	else
		printf("\nFALSE PART: Value of x is %d", x);//this is executed



	getch();
}