
#include <stdio.h>

int main(void)
{

	//using an if statement
	int x, y, max;
	x = 10;
	y = 5;

	if (x > y)
		max = x;
	else
		max = y;
	printf("The larger value is %d", max);	//using ternary operator	max = x > y ? x : y;  //Read:  If x>y ?(THEN) x :(ELSE) y		printf("\nThe larger value is %d", max);	getch();	//**********************************************************************	//Write a program to test whether a number is even or odd

	int num = 0;
	printf("Please enter a number :\n");
	scanf("%d", &num);
	//add validation to check only if number is greater than 0
	if (num > 0)
		if (num % 2 == 0) // the number divided by 2 leaves no remainder
			printf("The number is even\n");
		else
			printf("The number is odd\n");
	else
		printf("Number is negative.  Cannot check ");
	//re write the if statement using a ternary op

	
	num > 0 ? num % 2 == 0 ? printf("The number is even\n") : printf("The number is odd\n") : printf("Number is negative.  Cannot check ");
		//more nested ternary op	//what is the output of this program -- The output is c	int i = 5;
	char result = i % 2 == 0 ? 'a' : i % 3 == 0 ? 'b' : i % 5 == 0 ? 'c' : i % 7 == 0 ? 'd' : 'e';
	printf("Result is: %c ", result); 	getch();
}