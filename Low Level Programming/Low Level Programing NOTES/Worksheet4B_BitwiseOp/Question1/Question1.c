#include <stdio.h>
int main(void)
{
	/*
	1. Consider a binary bit pattern 0110 0011 (decimal value 99). Use a suitable
	mask and bitwise operator to reset the 2nd and 5th bit to obtain the bit
	pattern 0110 0001. Write a program to test your result.
	
	Solution:  Remember that when using a bitwise AND operator (&) we use 0 to reset input and 1 to retain input

	INPUT:  0110 0011 &
	MASK :  1110 1101
	-----------------
	OUTPUT: 0110 0001
	*/

	int input = 99; // 0110 0011
	int mask = 237; // 1110 1101

	printf("Resetting 2nd and 5th bit: %d ", input & mask); // 97 -->  0110 0001

	/*
	2. Write a program to reset the first 4 bits of a binary 8-bit pattern using a
	suitable mask and bitwise operator.
	For example, 1001 1101 becomes 1001 0000	Solution:	INPUT:  1001 1101 &
	MASK :  1111 0000
	-----------------
	OUTPUT: 1001 0000	*/
	input = 157;
	mask = 240;
	printf("Resetting first 4 bits: %d ", input & mask); // 144 --> 1001 0000


	/*3. Write a program to set the first 4 bits of a binary 8 - bit pattern using a
		suitable mask and bitwise operator.
		For example, 1001 1101 becomes 1001 1111		Solution:  Remember that we use a bitwise OR operator to set the input by using 1 and retaining input by using 0		INPUT:  1001 1101 |
		MASK :  0000 1111
		-----------------
		OUTPUT: 1001 1111		*/		input = 157;
		mask = 15;
		printf("Setting first 4 bits: %d \n", input | mask); // 159  --> 1001 1111	
	/*
	4. The 6th bit of the bit pattern of a letter of the alphabet determines whether it is lowercase or uppercase. For example,
			Letter �a� has bit pattern 0110 0001 --> 97
			Letter �A� has bit pattern 0100 0001 --> 65
		Write a program to prompt the user to enter a letter of the alphabet. Use a suitable bitwise operator and mask to invert the
		6th bit to change the case of the letter input.		Explanation:  The difference between the ascii code of caps letter and lowercase letter is 32, so if the 6th bit (2^6 = 32) is 0 (reset) then it is a capital letter		otherwise if it is set (1) then it is a lower case letter.		Solution:  Neither AND or OR can be used because we need to invert input. We cannot use a NOT because it will invert all input.  Therefore we need to use an XOR.		Remember that we use a 1 to invert input and 0 to retain value		INPUT:  0110 0001  ^ XOR  (a --> 97)
		MASK :  0010 0000  (32)
		-----------------
		OUTPUT: 0100 0001  (A - 65)
	*/
		char ch;
		mask = 32;  //0010 0000
		printf("Enter a letter \n");
		scanf("%c", &ch);
		printf("The letter you entered is %c.  Converting case.. %c \n", ch, ch ^ mask);
		getch();

		/*
		5. To represent a negative number in 2s complement representation we first
		apply the 1s complement and then add 1 to the result. Write a program to
		convert a number to negative
		(i) by using the ~ one�s complement operator
		
		Soln:
		 To represent -7 in 2's complement
		  represent 7 in binary -->      0000 0111
		  find the 1's complement-->     1111 1000  (1's complement op ~)
		  add 1                                + 1
		                                 __________
										 1111 1001  (-7 in 2's complement)

		(ii) by applying a suitable mask with an XOR bitwise operator ^
			 NOTE that an XOR bitwise operator can be used like a 1's compl op (~) by XORing the input with 1
			 A XOR 1 = ~A = NOT A

			 -7 ---> 0000 0111 XOR
					 1111 1111
					 _________
					 1111 1000 (1's complement)
						   + 1
					 _________
					 1111 1001 (-7 in 2's complement)
		*/

		printf("Enter a number greater than 0 ");
		int num;
		scanf("%d", &num);
		if (num > 0)
		{
			printf("Converting number to negative using 1's complement operator: %d\n", ~num + 1);
			printf("Converting number to negative using XOR bitwise operator: %d\n", (num ^ -1) + 1);
			//Note that we are using -1 as the second input for the XOR operation so that we fill up the bit pattern with 1s
		}
		else
		{
			printf("Enter a number greater than 0 ");
		}

		getch();
}