#include <stdio.h>
#define MASK 18 //0001 0010
#define MAX 50
int main(void)
{

	//to encrypt apply an XOR mask to flip eg 2nd and 5th bit of each character within the string

	char sentence[MAX];
	char encrypted[MAX];
	char decrypted[MAX];

	int i = 0;
	
	printf("Enter some text to encrypt");
	scanf("%49[^\n]", sentence); //get text from user until limit is reached or enter key is pressed

	while (sentence[i] != '\0' && i<MAX)   
	{
		encrypted[i] = sentence[i] ^ MASK;
		i++;
	}
	encrypted[i] = '\0';  //IMPORTANT: DO NOT FORGET TO APPEND THE NULL CHARACTER TO END OF YOUR STRING

	printf("Original: %s\n", sentence);
	printf("Encrypted sentence %s\n", encrypted);

	//to decrypt simply apply the same XOR mask to the encrypted string

	i = 0;
	while (encrypted[i] != '\0')
	{
		decrypted[i] = encrypted[i] ^ MASK;
		i++;
	}
	decrypted[i] = '\0';
	printf("Decrypted sentence %s\n", decrypted);

	//to get XOR cipher key simply perform:  original XOR encrypted -- This is called known-plaintext attack
	int key = sentence[0] ^ encrypted[0];
	printf("The XOR key is %d \n", key);


	getch();
}