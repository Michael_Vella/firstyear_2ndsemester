#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

int main(void)
{
	//Note that atof() and atol() also exist -- see notes 
	// note that atof() returns double :)

	char str[] = "4812866868686868686865464649886";
	int num = atoi(str); //accepts a string and converts it to int. Returns 0 if unsuccessful

	//if you do not detect overflow, the largest possible int value (MAXINT) will be stored
	// behaviour is undefined (depends on compiler and machine)
	if (errno == ERANGE) //overflow error 
		printf("Your number is out of range ");
	else if (num == 0)
		printf("Your data is not numeric ");
	else
		printf("Your number is : %d", num);

	getch();


}