#include <stdio.h>

int main(void)
{
	//count to 100
	//BAD example:
	for (int i = 1; i <= 1000; i++)
	{
		printf("%d ", i);
		if (i = 100)
			break; //avoid using breaks in loops - write down a proper condition in the ()
	}

	//classic infinite loop in C
	/*for (;;)
	{
		printf("hello");
	}*/
	
	//ask user to enter 100 numbers or until -1 is pressed
	//ALWAYS DO THESE 3 CHECKS when writing loops
	/*    1--> is the control variable initialised?
		  2--> is the condition which controls the loop correct? 
					Remember --> condition must be true to loop the loop :)
		  3--> is there code within the loop which will, at some point, set the condition to false ?
	*/
	int j=0, num = 0;
	while (j<100 && num!=-1)
	{
		scanf("%d", &num);
		j++;
	}
	//convert it to a for loop
	for (j = 0, num=0; j < 100 && num != -1; j++)
	{
		scanf("%d", &num);
	}
	//convert it into a do while
	j=0;
	do {
		scanf("%d", &num);
		j++;
	} while (j < 100 && num != -1);
	getch();
	
	// Worksheets 5 (ifs), 6 (loops) and 7 (string)
}