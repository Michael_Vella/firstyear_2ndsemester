#include <stdio.h>
int main(void)
{
	int a = 15; //0000 0000 0000 0000 0000 0000 0000 1111  (Remember that since integer is 4 bytes we need 32 bits!!)
	int b = 35; //0000 0000 0000 0000 0000 0000 0010 0011

	printf("(Bitwise AND) The value of %d & %d is %d\n", a, b, a&b); //0000 0011 -> 3
	printf("(Bitwise OR)  The value of %d | %d is %d\n", a, b, a|b); //0010 1111 -> 47
	printf("(Bitwise XOR) The value of %d ^ %d is %d\n", a, b, a^b); //0010 1100 -> 44
	printf("(Bitwise NOT) The value of NOT A is %d\n", ~a);			//1111 0000 -> -16  Note that this is a negative number because it starts with 1
	getch();


}