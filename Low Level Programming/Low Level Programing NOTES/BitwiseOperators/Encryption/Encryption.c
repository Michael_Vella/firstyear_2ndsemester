#include <stdio.h>
#define CIPHERKEY 18 //0001 0010

int main(void)
{
	char letter = 'a'; 

	printf("The code of %c is %d\n", letter, letter); // 'a' has bit pattern 0110 0001
	//Remember that when one input of XOR is 1 the output is inverted

	//create a 'cipher key' to invert the 2nd bit and the 5th bit
	// our cipher key will be bit pattern 0001 0010 

	//To encrpyt a character:  letter ^ cipherkey --> encrypted letter
	char encrypted = letter ^ CIPHERKEY;
	printf("The letter %c is encrypted to %c \n", letter, encrypted);
	/*
		0110 0001 LETTER (a)
		0001 0010 CIPHERKEY
		---------
		0111 0011 ENCRYPTHED CHAR (s)
	*/

	//To decrypt an encrypted letter: encryptedletter ^ cipher key ---> original letter
	char original = encrypted ^ CIPHERKEY;
	printf("The original letter %c\n", original);

	//To obtain the cipher key: encrypted ^ decrypted --> ciper key
	char cipher = encrypted ^ original;
	printf("The cipher key is %d\n", cipher);
	//HW -- try out worksheet 4B on moodle
	getch();
}