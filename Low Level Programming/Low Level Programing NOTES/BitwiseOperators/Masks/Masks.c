/*
	A	B	A AND B
	0	0		0	-> when B is 0 output is reset (0)
	0	1		0	-> when B is 1 retain value of A
	1	0		0	-> when B is 0 output is reset (0)
	1	1		1	-> when B is 1 retain value of A


How to use an AND & operator with masks
Use a 0 in a mask to reset output.  Use a 1 in the mask to let the input through.

*/

/*
How to use OR in a mask - 
use 1 in a mask to set output. Use 0 in the mask to let the input through
A	B	A OR B
0	0	  0		-> when B is 0 retain value of A
0	1	  1		-> when B is 1 output is set(1)
1	0	  1		-> when B is 0 retain value of A
1	1	  1		-> when B is 1 output is set(1)
*/

/*
How to use XOR in a mask
Use 1 in a mask to invert the input.  Use 0 in the mask to retain the value of A
Note that when the two inputs are the same the output is 0 and when the two inputs are different the output is 1
The XOR can therefore be used to compare bit patterns.

A	B	A XOR B
0	0	  0		-> when B is 0 retain value of A
0	1	  1		-> when B is 1 invert value of A
1	0	  1		-> when B is 0 retain value of A
1	1	  0		-> when B is 1 invert value of A
*/



#include <stdio.h>
int main(void)
{
	int num1 = 74;  // 0100 1010
	int mask = 240; // 1111 0000
	
	// write code to RESET (0) the first 4 bits of an 8-bit pattern
	printf("To reset the first 4 bits of an 8-bit pattern: %d\n", num1 & mask); //64
	/*
	0100 1010  &
	1111 0000
	----------
	0100 0000 
	*/

	//write code to SET (1) the last 4 bits of an 8-bit pattern
	/*
	0100 1010  |
	1111 0000
	---------
	1111 1010
	*/
	printf("To set the last 4 bits of an 8-bit pattern: %d\n", num1 | mask); //250

	
	//You can use an XOR bitwise operator to check which bits in 2 bit patterns are different
	//the ouput of an XOR gives 1 when the inputs are different and 0 when they are the same
	int num2 = 74;
	printf("To check if 2 bit patterns are the same: %d\n", num1 ^ num2); //gives 0 because all bits are the same

	getch();
}



