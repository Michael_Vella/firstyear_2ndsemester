#include <stdio.h> 
#define ROWS 4
#define COLS 6
int main(void) { //declare a 2D array of strings and initialise it 
	
	char * seatingPlan[ROWS][COLS] =
	{ 
		{ "amy", "tom", "eli", "paul", "joe", "lara" },
		{ "red", "greg", "mark", "joe", "vic", "bet" }, 
		{ "bob", "carl", "mary", "rick", "sam", "cloe" }, 
		{ "rob", "alex", "ben", "ann", "rose", "sally" }
	}; //use a nested for loop to traverse the 2D array 
	
	for (int row = 0; row < ROWS; row++)	
	{ 
		for (int col = 0; col < COLS; col++)//display one row at a time
			{ printf("%10s", seatingPlan[row][col]);
	} 
		printf("\n"); //skip a line after every row 
	} 
	printf("%s", seatingPlan[0, 3]); //displays the 3rd student in first row eli
	getch(); 
}