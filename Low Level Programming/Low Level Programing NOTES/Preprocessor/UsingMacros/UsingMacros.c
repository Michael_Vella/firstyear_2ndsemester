#include <stdio.h>
#define PI 3.1415 //the symbolic name PI will be substituted to 3.1415 during the preprocessing stage
#define PRINTME printf("\nHello")  //this is a macro; PRINTME will be substituted with printf("\nHello")
#define SQUARE(X) ((X)*(X)) //NOTE that the () are extremly important in macros because of operator precedence (BODMAS)
#define SQ(X) (X*X)  //this macro is wrong!!

int main()
{
	//if you use CONST it will be substituted during the compilation phase
	const int max = 5; //another way how to declare a constant but the #define is preferred

	float radius, area;
	printf("Enter the radius: ");
	scanf("%f", &radius);
	
	area = PI * radius * radius;
	printf("Area %f", area);
	PRINTME;

	printf("\nSQUARE(2) is %d ",SQUARE(2));
	/*
		SQUARE(2) ---> ((2) * (2))
	*/
	printf("\nSQUARE(2+1) is %d ", SQUARE(2+1));
	/*
	SQUARE(2+1) ---> ((2+1) * (2+1))
				---> ((3) * (3))
	*/
	printf("\nSQ(5) is %d", SQ(5)); //25  --> SQ(5) = (5*5) = 25
	printf("\nSQ(5+2) is %d", SQ(5 + 2)); //??? --> SQ(5+2) = (5+2 * 5+2) = 17 NOT 49 as expected

	getch();
	return 0;
}