
#define PI 3.1415 //the symbolic name PI will be substituted to 3.1415 during the preprocessing stage
#define PRINTME printf("\nHello")  //this is a macro; PRINTME will be substituted with printf("\nHello")
#define SQUARE(X) ((X)*(X)) //NOTE that the () are extremly important in macros because of operator precedence (BODMAS)
#define SQ(X) (X*X)  //this macro is wrong!!
