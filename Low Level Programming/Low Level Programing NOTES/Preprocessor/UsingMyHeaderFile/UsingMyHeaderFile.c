#include "Myheader.h" //note how the order of the includes in not important :)
#include <stdio.h>

int main()
{
	//if you use CONST it will be substituted during the compilation phase
	const int max = 5; //another way how to declare a constant but the #define is preferred

	float radius, area;
	printf("Enter the radius: ");
	scanf("%f", &radius);

	area = PI * radius * radius;
	printf("Area %f", area);
	PRINTME;

	printf("\nSQUARE(2) is %d ", SQUARE(2));
	/*
	SQUARE(2) ---> ((2) * (2))
	*/
	printf("\nSQUARE(2+1) is %d ", SQUARE(2 + 1));
	/*
	SQUARE(2+1) ---> ((2+1) * (2+1))
	---> ((3) * (3))
	*/
	printf("\nSQ(5) is %d", SQ(5)); //25  --> SQ(5) = (5*5) = 25
	printf("\nSQ(5+2) is %d", SQ(5 + 2)); //??? --> SQ(5+2) = (5+2 * 5+2) = 17 NOT 49 as expected

	getch();
	return 0;
}