#include <stdio.h>

// using typedef to define a new datatype of type struct book
typedef
struct book {
	int bookId;
	char isbn[13];
	char title[50];
	char author[50];
} bookType;


int main(void)
{
	bookType myBook;
	FILE *ptr_myfile;
	
	//code to read from file
	ptr_myfile = fopen("test.bin", "rb");
	if (!ptr_myfile)
	{
		printf("Unable to open file!");
		return 1;
	}
	else
	{
		fread(&myBook, sizeof(bookType), 1, ptr_myfile);
		printf("The bookid is %d\n", myBook.bookId);
		printf("The ISBN is %s\n", myBook.isbn);
		printf("The book title is %s\n", myBook.title);
		printf("The book author is %s\n", myBook.author);
	}
	fclose(ptr_myfile);
	return 0;
}