#include <stdio.h>

// using typedef to define a new datatype of type struct book
typedef
struct book {
	int bookId;
	char isbn[13];
	char title[50];
	char author[50];
} bookType;

int main(void)
{
	
	bookType myBook = { 13452,
		"0752063326961",
		"C Primer Plus",
		"Stephen Prata"
	};
	
	FILE *ptr_myfile;
	
	//code to write to file
	ptr_myfile = fopen("test.bin", "wb");
	if (!ptr_myfile)
	{
		printf("Unable to open file!");
		return 1;
	}
	else
	{
		fwrite(&myBook, sizeof(bookType), 1, ptr_myfile);
	}
	fclose(ptr_myfile);
	/******************************************************************************/

}