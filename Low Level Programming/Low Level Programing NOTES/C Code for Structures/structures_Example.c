#include <stdio.h>

// using typedef to define a new datatype of type struct book
typedef
	struct book {
		int bookId;
		char isbn[13];
		char title[50];
		char author[50];
} bookType;


int main(void)
{
	//declaring a structure called mybook and initialising it
	bookType myBook = { 13452,
				"0752063326961",
				"C Primer Plus",
				"Stephen Prata"
				};

	//to access each field use the . notation  For example:  myBook.bookId

	printf("The bookid is %d\n", myBook.bookId);
	printf("The ISBN is %s\n", myBook.isbn);
	printf("The book title is %s\n", myBook.title);
	printf("The book author is %s\n", myBook.author);
	//*******************************************************************************************
	
	bookType myBookArray[3];
	//fill up the array at runtime
	for (int i = 0; i < 3; i++)
	{
		printf("Entering book information: %d\n", i);
		printf("Book Id : ");
		scanf("%d", &myBookArray[i].bookId);
		printf("ISBN : ");
		scanf("%s", &myBookArray[i].isbn); //or use fgets()
		printf("Title : ");
		scanf("%s", &myBookArray[i].title);
		printf("Author : ");
		scanf("%s", &myBookArray[i].author);
	}

	//display the contents of your array

	for (int i = 0; i < 3; i++) {
		printf("Book Id : %d\n", myBookArray[i].bookId);
		printf("ISBN : %s\n", myBookArray[i].isbn);
		printf("Title : %s\n", myBookArray[i].title);
		printf("Author : %s\n", myBookArray[i].author);
	}
	getch();
}