#include <stdio.h>

//use typedef to define a new data type
// syntax:  typedef <old type> <new type name>
typedef char * STRING;
int main(void)
{
	//declaring an array of strings as an array of array of 20 chars	char nameList[5][20] =	{ "Toni", "Claire", "Simon", "Peppa", "Jessie"};	//write code to display the contents of the array	for (int i = 0; i < 5; i++)	{		printf("\nThe name at position %d is %s", i, nameList[i]);	}//***********************************************************************************************	//another way how to declare an array of strings --> as an array of pointer to char	char * otherList[5] = {"Toni","Claire", "Simon", "Peppa", "Jessie" };	//write code to display the contents of the array	for (int i = 0; i < 5; i++)	{		printf("\nThe name at position %d is %s", i, otherList[i]);	}//***********************************************************************************************	//declare the array of strings using new data type STRING	STRING yetAnotherList[5] = {"Toni","Claire", "Simon", "Peppa", "Jessie" };
	//write code to display the contents of the array
	for (int i = 0; i < 5; i++)	{		printf("\nThe name at position %d is %s", i, yetAnotherList[i]);	}

	//write code to fill up the arrays at runtime using scanf()
	for (int i = 0; i < 5; i++)
	{
		printf("\nEnter a name: ");
		scanf("%19s", &nameList[i]);   //note that we use 19 with the modifier so that scanf captures only the first 19 chars from buffer - leaving 1 space for the null char \0
	}
	//display
	getch();
}