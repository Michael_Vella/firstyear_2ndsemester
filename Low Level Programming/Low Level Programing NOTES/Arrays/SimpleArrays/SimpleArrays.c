#include <stdio.h>
#define SIZE 5

int main(void)
{
	//declare an array of 5 integers
	int numArray[SIZE] = { 10, 20, 30, 40, 50 };
	
	//How to display the contents of the array
	for (int i = 0; i < SIZE; i++)
	{
		printf("\nValue at location %d is %d", i, numArray[i]);
	}
	
	//What happens when you go out of bounds
	/*
	numArray[5] = 100; //trying to access 6th element
	printf("\nThe value is : %d ", numArray[5]);
	*/
	//the compiler does not complain when you try to access a cell which is out of bounds
	//however a runtime error is then reported
	//

	//You have to calculate the size of the array yourself
	printf("\nThe number of bytes used by the array is %d", sizeof(numArray));
	printf("\nThe number of bytes used by one element %d", sizeof(numArray[0]));
	int numberOfElements = sizeof(numArray) / sizeof(numArray[0]);

	printf("\nThe number of elements in the array is %d", numberOfElements);


	getch();

}
