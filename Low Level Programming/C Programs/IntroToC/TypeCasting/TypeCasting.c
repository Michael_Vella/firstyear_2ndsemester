#include <stdio.h>

int main(void) 
{

	int num = 123456;
	long long lnum = 123352436453454;
	num = lnum; // what happens if we store a value bigger thatn the receiveing variable 

	printf("Value: %d \n", num);

	float fNum = 1223.15f;
	num = fNum;// what happens if we try to sotre a real numbver in an integer
	printf("Value: %d\n", num);

	char c = 'a';
	num = c; // what happens if we try to store a real number in an integer
	printf("Value: %d\n", num);

	float answer = (float)num / fNum;

	getch();
	return 0;
}