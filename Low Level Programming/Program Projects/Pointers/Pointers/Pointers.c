#include <stdio.h>

int main(void)
{
	int num = 50;
	printf("The value of num is: %d \n", num);
	printf("The memory address of num is: %x\n", &num);

	char ch = 'a';
	printf("The value of ch is: %c\n", ch);
	printf("The memory address of num is: %x \n", &ch);
	
	//how to use the '&' operator to get data from the keyboard
	int num2;
	printf("Enter a number: \n");
	scanf("%d", &num2);
	printf("The number you entered is %d \n", num2);
	
	//using pointers
	//declare an int var with value of 40
	int num3 = 40;
	//declare pointer to int iPtr
	int *iPtr;
	//set pointer to point to int var
	iPtr = &num3;
	printf("The value of the variable pointed to by iPtris : %d\n", *iPtr);
	printf("The address of the variable: %d\n", &num3);
	printf("The address of the variable pointed to by iPtris : %d\n", &*iPtr);
	getch();
	return 0;
}