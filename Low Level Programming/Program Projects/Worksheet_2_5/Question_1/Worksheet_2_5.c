#include <stdio.h>

int main(void)
{
	/*
	1)
	Declare the following the pointers:
		(a) iPtr pointer to integer
		(b) cPtr pointer to character
		(c) fPtr pointer to float
		(d) ipp pointer to pointer to integer
	*/
	//a
	int *iPtr;
	//b
	char *cPtr;
	//c
	float *fPtr;
	//d
	int **ipp;

	/*
	2)
	Write a program to do the following:
	(a) Declare a pointer to integer, called ip and set it to NULL.
	(b) Declare an integer called number and set a value in it.
	(c) Set your pointer to point to the variable number.
	(d) Display the contents of the pointer ip. What does this value represent?
	(e) Display the value of the variable to which the pointer ip is pointing (use only ip variable).
	(f) Display the address of the pointer variable, ip.
	(g) Change the value of the variable number by using only the pointer ip.
	(h) Display the new contents of the variable number.
	*/
	//a
	int *ip = NULL;
	//b
	int number = 2;
	//c
	ip = &number;
	//d
	printf("Pointer 'ip' contents: %p", ip);//displays the memory addresso of number
	//e
	printf("\nPointer 'ip' value : %d", *ip);// displays the value of the 'number'
	//f
	printf("\nPointer 'ip' memory address: %p", ip);//displays the memory address of the pointer
	//g
	*ip = 4;
	printf("\nPointer 'ip' value : %d", *ip);
	//h
	printf("\nThe bit size of the pointer 'ip': %d", sizeof(*ip));
	
	/*
	3)
	Write a program to do the following:
	(a) Declare two pointers to character, cPtr1 and cPtr2.
	(b) Declare a character variable ch and store the value �A�;
	(c) Set the pointer cPtr1 to point to ch.
	(d) Set cPtr2 to store the contents of cPtr1.
	(e) Write code to show that the two pointers are now pointing to the same variable.
	*/
	//a
	char *cPtr1;
	char *cPtr2;
	//b 
	char ch = 'A';
	//c
	cPtr1 = &ch;
	//d
	cPtr2 = cPtr1;
	//e
	printf("\ncPtr1 pointing to memory address: %c", *cPtr1);
	printf("\ncPtr2 pointing to memory address: %c", *cPtr2);
	
	/*
	4)
	Write a program to do the following:
	(a) Set a pointer to point to a integer variable;
	(b) Display the contents of the pointer.
	(c) Increment the pointer by 1. What happens? Why?
	(d) Can you display the contents of the memory cell to which the pointer is now pointing?
	(e) Can you change the contents of the memory cell to which the pointer is now pointing?
	*/
	//a
	int *iPtr1;
	int in = 0;
	iPtr1 = &in;
	//b
	printf("\nContents of iPtr: %p", iPtr1);
	//c
	iPtr1 += 1;
	printf("\nContents of iPtr: %d ", *iPtr1);
	//d 
	printf("\nContents of iPtr: %p ", iPtr1); //different memory address
	//e
	printf("\nValue of iPtr1:%d ", *iPtr1);

	/*
	5)
	Write a program to do the following:
	(a) Declare a pointer to integer numPtr.
	(b) Set a value to the address pointed to by numPtr. What happens? Why?
	(c) Can we store a memory address in the pointer variable directly?
	(d) What happens if you try to change the contents of this memory address? Why?
	*/
	//a
	int num;
	int *numPtr;
	//b
	numPtr = &num;
	//c

	getch();
	return 0;
}