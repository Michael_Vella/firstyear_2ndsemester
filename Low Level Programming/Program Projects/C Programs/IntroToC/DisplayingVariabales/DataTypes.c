#include <stdio.h>

int main(void) 
{
	int iNum = -100;
	short int sNum = 90;
	long int lNum = 1234567;
	unsigned int uNum = 123;
	long long int veryLongNum = 81481814;
	float fNum = 1.234f;
	double dNum = -1.2345e09;
	long double veryLongDouble = 0.112312e-99;
	char c = 'a';
	_Bool hungry = 1;

	printf("Integer: %d\n ", iNum);
	printf("Short Integer: %hd \n", sNum);
	printf("Long Integer: %ld \n",lNum);
	printf("Very Long Integer: %lld \n", veryLongNum);
	iNum = 21;
	printf("Decimal: %d Ocatal: %o Hexadecimal: %x \n", iNum,iNum, iNum);
	printf("Decimal: %#d Ocatal: %#o Hexadecimal: %#x \n", iNum, iNum, iNum);

	printf("Real numbers: %f and %e and %le \n", fNum, dNum, veryLongDouble);
	printf("Character: 5c \n", c);

	printf("This is a character %c. Its ascii code is %d", c,c);
	printf("Float value %f Double value %f", fNum, dNum);
	getch();
	return 0;
}