#include <stdio.h>
#include <limits.h>

int maint(void) 
{
	//using 1's compliment
	printf("Minimum Signed Int  %d\n",-(int)((unsigned int)~0 >>1) -1);
	printf("Maximum Signed Int %d\n", (int)((unsigned int)~0 >> 1));

	//Aaron method for minimum negative value
	//fill up with 1s, shift all to left except last bit

	printf("Aaron method min signed %d\n", -(char)((unsigned char)~0 >> 1) -1);

	printf("Minimum Signed Char %d\n", -(char)((unsigned char) ~ 0 >>1)-1);
	printf("Maximum Signed Char %d\n", -(char)((unsigned char)~0 >> 1));
	
	printf("");
	//using 2's compliment
	printf("Minimum signed int = %d \n", INT_MIN);
	printf("Maximum signed int = %d \n", INT_MAX);
	
	printf("The minmum value of an unsigned char = ", 0);
	printf("The maximum value of a usigned char = ", UCHAR_MAX);

	printf("The minimum value of a signed char =  " ,SCHAR_MIN);
	printf("The maximum value of a signed char =  ", SCHAR_MAX);
	getch();
}

