#include <stdio.h>

int main(void)
{
	char name[40];
	char praise[] = "You are an extroadinary being.";

	printf("What is your name?");
	scanf("%s", name);
	printf("\nHello, %s. %s\n", name, praise);
	printf("Your name is %d characters long but occupied %d cells in memory.", strlen(name), sizeof(name));

	//EXAMPLE 2
	//char name[30], ch = ' ';
	//int i = 0;
	//prinf("Enter name: ");
	//while (ch != '\n')
	//{
	//	ch = getchar();
	//	name[i] = ch;
	//	i++;
	//}
	//name[i] = '\0'; //inserting the null character 
	//printf("Your name %s", name);

	getch();
	return 0;
}