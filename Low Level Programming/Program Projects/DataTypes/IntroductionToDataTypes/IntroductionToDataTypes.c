#include <stdio.h> 

int main(void) {
	short shortNum;
	int intNum;
	long longNum;
	long long longlongNum;
	float floatNume;
	double doubleNum;
	long double longDNum;

	printf("Type short has %zd bytes \n", sizeof(shortNum));
	printf("Type int has %zd bytes \n", sizeof(intNum));
	printf("Type long has %zd bytes \n",sizeof(longNum));
	printf("Type Long Long has %zd bytes\n", sizeof(longlongNum));
	printf("Type float has %zd bytes\n", sizeof(floatNume));
	printf("Type double has %zd bytes\n", sizeof(doubleNum));
	printf("Type Long Double has %zd bytes\n", sizeof(longDNum));
	getch();
	return 0;
}