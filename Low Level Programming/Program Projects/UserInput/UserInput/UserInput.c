#include <stdio.h>
#define _CRT_NO_WARNINGS
int main(void)
{
	//int age;
	//float weight;
	//char pet[30];

	//printf("Enter your age, weight and pet's name\n\n");
	//
	//printf("Age: ");
	//scanf("%d", &age);

	//printf("\nWeight: ");
	//scanf("%f", &weight);

	//printf("Pet's name: ");
	//scanf("%s", pet); // Imp you don't need to use a '&' for a char array
	//				  //The 'pet' equivalent '&pet[0]'
	//printf("\n===========================\n");

	//printf("You are %d years old.\n Your weight is %.2f KG and your pet's name is %s", age, weight, pet);

	//Scanf stops taking information when the user enters a whitespace such a
	//tab, space, or enter
	//NOTE: the function scanf() should not be used for char datatypes

	/*printf("Enter a number:");
	int num = 0 ;
	scanf("%d", &num);
	printf("%d\n",num);

	printf("Checking Buffer: \n");
	char str[50];
	scanf("%s", str);
	printf("Buffer: %s", str);

	printf("\nEnter a number:");
	int num2 = 0;
	scanf("%d", &num2);
	printf("%d\n", num2);

	printf("Checking Buffer: \n");
	char str1[50];
	scanf("%s", str1);
	printf("Buffer: %s", str1);

	printf("\nEnter a number:");
	int num3 = 0;
	scanf("%d", &num3);
	printf("%d\n", num3);

	printf("Checking Buffer: \n");
	char str2[50];
	scanf("%s", str2);
	printf("Buffer: %s", str2);*/

	char fullname[50];
	printf("Pet's name: ");
	scanf("%s[^\n][^\t][^\r]", fullname);
	printf("%s", fullname);
	getch();
	return 0;

}