/*
	TOPIC 2 - User Defined Functions
*/
USE IICT6203;
GO
--Q1 Show the current salary for the Employee with The ID of 1
SELECT Salary
FROM [Topic2].[JobHistory]
Where [Employee Id] = 1 AND [End Date] IS NULL;
GO
--Q2 -- Create a function that returns the current salary for a given employee
CREATE FUNCTION Topic2.udf_getEmpSalary(@empID INTEGER)
RETURNS INTEGER 
AS 
	BEGIN 
		DECLARE @currentSalary

		SELECT @currentSalary = jobHis.Salary
		FROM [Topic2].[JobHistory] jobHis
		Where [Employee Id] = @empID AND [End Date] IS NULL;
		
		RETURN @currentSalary;
	END;
GO
--Q3 --Test the nwe function with the employee ID 1
SELECT Topic2.udf_getEmpSalary(1);
GO
--Q4 --Create a function that returns how many people started working a
-- particular job for a given year
CREATE FUNCTION Topic2.udf_getEmpYear (@year INT)
RETURNS INTEGER
AS 
	BEGIN 
		DECLARE @numEmp INTEGER;

		SELECT @numEmp = Count(*)
		FROM Topic2.JobHistory
		WHERE YEAR([From Date]) = @year;

		RETURN @numEmp;
	END;
GO

--Q5 Test the above function
SELECT Topic2.udf_getEmpYear(2000) AS 'Total Employees';
GO

SELECT *,[From Date], YEAR([From Date])
FROM Topic2.JobHistory
WHERE Year([From Date]) = 2000;
GO

--Q6 -- Show the department name, job title, from date and end date 
-- for specific employee

SELECT
	  dep.[Department Name],
	  job.[Job Title],
	  jobHis.[From Date],
	  jobHis.[End Date]
FROM Topic2.Employee emp
	 JOIN Topic2.JobHistory jobHis
	 ON (emp.[Employee Id] = jobHis.[Employee Id])
	 JOIN Topic2.Job job
	 ON (jobHis.[Job Id] =  job.[Job Id])
	 JOIN Topic2.Department dep
	 ON (job.[Department Id] = dep.[Department Id])
WHERE jobHis.[Employee Id] =1;
GO

--Q7 --Create a function that returns a table with the job history for a given EMPLOYEE

/*
--Transact-SQL Inline Table-Valued Function Syntax 
CREATE FUNCTION [ schema_name. ] function_name 
( [ { @parameter_name [ AS ] [ type_schema_name. ] parameter_data_type 
    [ = default ] [ READONLY ] } 
    [ ,...n ]
  ]
)
RETURNS TABLE
    [ WITH <function_option> [ ,...n ] ]
    [ AS ]
    RETURN [ ( ] select_stmt [ ) ]
[ ; ]

*/

CREATE FUNCTION Topic2.udf_EmpHistory (@empID INTEGER)
RETURNS TABLE 
AS 
RETURN 
(
SELECT
	  dep.[Department Name],
	  job.[Job Title],
	  jobHis.[From Date],
	  jobHis.[End Date]
FROM Topic2.Employee emp
	 JOIN Topic2.JobHistory jobHis
	 ON (emp.[Employee Id] = jobHis.[Employee Id])
	 JOIN Topic2.Job job
	 ON (jobHis.[Job Id] =  job.[Job Id])
	 JOIN Topic2.Department dep
	 ON (job.[Department Id] = dep.[Department Id])
WHERE emp.[Employee Id] = @empID
);
GO

SELECT * FROM Topic2.udf_EmpHistory(1);

--Q9 -Revise the previous function to accept a parameter used to determine 
--whether to include the current job.

DROP FUNCTION Topic2.udf_EmpHistory;
GO

CREATE FUNCTION Topic2.udf_EmpHistory(@empID INT ,@includeCurrent BIT)
RETURNS @EmpJobHistory TABLE 
([Department Name] NVARCHAR (45),
 [Job Title] NVARCHAR (45),
 [From Date] DATE,
 [End Date]  DATE)
BEGIN
	IF (@includeCurrent = 1)
		BEGIN 
		--QUERY TO INSERT JOB HISTORY WITH CURRENT JOB
			INSERT INTO @EmpJobHistory([Department Name], [Job Title], [From Date], [End Date])
			SELECT dep.[Department Name],
			 job.[Job Title],
			 jobHis.[From Date],
			 jobHis.[End Date]
			FROM Topic2.Employee emp
			 JOIN Topic2.JobHistory jobHis
			 ON (emp.[Employee Id] = jobHis.[Employee Id])
			 JOIN Topic2.Job job
			 ON (jobHis.[Job Id] =  job.[Job Id])
			 JOIN Topic2.Department dep
			 ON (job.[Department Id] = dep.[Department Id])
			WHERE emp.[Employee Id] = @empID;
		END
	ELSE
		BEGIN
		--QUERY TO INSERT JOB HISTORY WITHOUT CURRENT JOBa
		INSERT INTO @EmpJobHistory([Department Name], [Job Title], [From Date], [End Dte])
			SELECT dep.[Department Name],
			 job.[Job Title],
			 jobHis.[From Date],
			 jobHis.[End Date]
			FROM Topic2.Employee emp
			 JOIN Topic2.JobHistory jobHis
			 ON (emp.[Employee Id] = jobHis.[Employee Id])
			 JOIN Topic2.Job job
			 ON (jobHis.[Job Id] =  job.[Job Id])
			 JOIN Topic2.Department dep
			 ON (job.[Department Id] = dep.[Department Id])
			WHERE emp.[Employee Id] = @empID AND jobHis.[End Date] IS NOT NULL;
		END
	RETURN
END;
GO

--Q10 Test the previous function with employee 1 first without the current job 
-- and then with current job 

select * from Topic2.udf_EmpHistory(1,0);
select * from Topic2.udf_EmpHistory(1,1);

--Q11 Display the name, surname and job history for each employee using the previous
-- function
SELECT emp.[First Name], emp.[Last Name],
	   job.[Department Name], job.[Job Title],
	   job.[From Date], job.[End Date]
FROM Topic2.Employee emp
	 CROSS APPLY Topic2.udf_EmpHistory(emp.[Employee Id], 0) job

-- Q12 Revise the previous statment to includde all employees even those that don't have 
-- a previous job.
SELECT emp.[First Name], emp.[Last Name],
	   job.[Department Name], job.[Job Title],
	   job.[From Date], job.[End Date]
FROM Topic2.Employee emp
	 OUTER APPLY Topic2.udf_EmpHistory(emp.[Employee Id], 0) job

--Q13 CREATE a function named udf_JobsSalaryRange that returns a table
-- with the job title, salary and departmenty name for all the jobs
--that have been worked by a chosen employee.
-- The Resulting jobs must also have a salary that falls between a specified range.
CREATE FUNCTION Topic2.udf_JobsSalaryRange (@empID, @salaryMin INT, @salaryMAX INT)
RETURNS TABLE 
AS 
RETURN
(
SELECT job.[Job Title],
	   jh.[Salary],
	   de.[Department Name]
FROM Topic2.Department de
     JOIN Topic2.Job job
	 ON (de.[Department ID) = job.[Department ID]
	 JOIN Topic2.JobHistory jh
	 ON (job.[Job Id] = jh.[Job Id])
WHERE jh.[Employee Id] @empID
	  AND jh.Salary BETWEEN @salaryMin AND @salaryMax
);
GO
--Q14 Use the previous function to show all the matching details for all employees with
-- jobs not earning more than 15,000
SELECT * FROM Topic2.udf_JobsSalaryRange(emp.[Employee ID], 0, 15000);
GO