/*
	WORKSHEET 3 - FUNCTIONS
*/

--1 Using proper DB [CompanyDb];
GO

--Q1 Create a function that returns a table with the total number of 
--people and their total salary for a given department name. --Name the function udf_SalaryDepartment.CREATE FUNCTION dbo.udf_SalaryDepartment (@depName NVarchar (80))
RETURNS TABLE 
AS 
RETURN
(
SELECT Sum(emp.Salary) AS 'Total Salary',
	   COUNT(emp.[Employee Id]) AS 'Total Employee',
	   dep.[Department Name]
FROM Employee emp 
	 INNER JOIN Department dep
	 ON (emp.[Department Id] = dep.[Department Id])
WHERE dep.[Department Name] = @depName
GROUP BY dep.[Department Name]
);
GO
--Q2 Test the new function with the value ‘Marketing’
SELECT * FROM dbo.udf_SalaryDepartment('Marketing');
GO

--Q3 Create a function that returns how many departments are 
--found in a given country. Name the function udf_CountryDepts.CREATE FUNCTION dbo.udf_CountryDepts (@couName NVARCHAR (45))
RETURNS INT 
AS 
	BEGIN 
	DECLARE @deptCount INT
		SELECT  @deptCount  = COUNT(dep.[Department Name]) 
		FROM [dbo].[Department] dep 
		 INNER JOIN [dbo].[Location] loc
		 ON (dep.[Location Id] = loc.[Location Id])
		 INNER JOIN [dbo].[Country] cou
		 ON(loc.[Country Id] = cou.[Country Id])
		WHERE cou.[Country Name] = @couName;
	RETURN @deptCount;
END;
GO
--Q4 Test the new function for the value Germany
SELECT dbo.udf_CountryDepts('Germany');
GO

--Q5 Create a function that accepts a salary and returns the 
--grade level. Name the function udf_GradeLevel
CREATE  FUNCTION dbo.udf_GradeLevel(@empSalary NUMERIC (8,2))
RETURNS CHAR
AS
	BEGIN 
		DECLARE @grade CHAR 
		SELECT @grade = [Grade Level]
		FROM [dbo].[Job Grade]
		WHERE  @empSalary BETWEEN [Lowest Sal] AND [Highest Sal];
	RETURN @grade;
END;
GO
DROP FUNCTION dbo.udf_GradeLevel;
GO
--Q6 Show the first name, last name, job title, 
--salary and grade level (using the previous function) for
--all employees. Sort the results by the grade level, and then by the salary
SELECT 
	   emp.[First Name],
       emp.[Last Name],
	   job.[Job Title],
	   emp.Salary,
	   dbo.udf_GradeLevel(emp.Salary) AS 'Grade Level'
	   
FROM Employee emp
     INNER JOIN  Job job
	 ON (emp.[Job Id] = job.[Job Id])
ORDER BY 5 ASC, 4 ASC;
GO
--Q7 Create a function that returns 'IsAManager' if  an employee is a manager or not
-- name the function udf_isManager.

select *
from Employee;

CREATE FUNCTION dbo.udf_isManager (@empId INTEGER)
RETURNS NVARCHAR(45)
BEGIN
	declare @manager integer = 
		(select COUNT([Employee Id])
		 from Employee
		 where [Manager Id] = @empId)
								
	if (@manager =0 ) 
		RETURN 'Not a Manager'


	RETURN 'Is a Manager';
END;
GO

--q8 TEST THE FUNCITON FROM THE PREVIOUS STATEMENT 
select [First Name],
	   [Last Name],
	   dbo.udf_isManager([Employee Id])
from Employee;

--q9 Show the department name, country name, total employees and total salary per department
--Make sure to use the function created in Question 1. Most populated departments should appear
--first 

SELECT dep.[Department Name]
	   country
from Department dep
