/*
	TSQL - Database Programming 2
	TOPIC: 1 - Views and Sequences
*/

/*
	Systnax for a VIEW
	
	Syntax for SQL Server and Azure SQL Database  
  
CREATE [ OR ALTER ] VIEW [ schema_name . ] view_name [ (column [ ,...n ] ) ]   
[ WITH <view_attribute> [ ,...n ] ]   
AS select_statement   
[ WITH CHECK OPTION ]   
[ ; ]  
  
<view_attribute> ::=   
{  
    [ ENCRYPTION ]  
    [ SCHEMABINDING ]  
    [ VIEW_METADATA ]       
}   

*/
USE [IICT6203];
GO
--Q1 Show all the different job titles per departmentSELECT jb.[Job Title],
SELECT dep.[Department Name], jb.[Job Title]
FROM [Topic1].[Job] jb
	 INNER JOIN [Topic1].[Department] dep
	 ON (jb.[Department ID] = dep.[Department ID])
ORDER BY 2 ASC, 1 ASC;
GO

--Q2 Enclose the previous in a view named view_Jobs
CREATE VIEW Topic1.view_JOBs 
AS	SELECT jb.[Job Title],
	   dep.[Department Name]
	FROM [Topic1].[Job] jb
	 INNER JOIN [Topic1].[Department] dep
	 ON (jb.[Department ID] = dep.[Department ID]) ;
GO

SELECT vjb.[Job Title],
	   vjb.[Department Name]
FROM Topic1.view_JOBs vjb
ORDER BY 2 ASC, 1 ASC;
GO 

DROP VIEW Topic1.view_JOBs;
GO

--q3 Re-create the previous view, rename the coloumns as job and department
CREATE VIEW Topic1.view_Jobs ([Job], [Department])
AS SELECT jb.[Job Title],
	   dep.[Department Name]
	FROM [Topic1].[Job] jb
	 INNER JOIN [Topic1].[Department] dep
	 ON (jb.[Department ID] = dep.[Department ID]);
GO

SELECT vjb.Job,
	   vjb.Department
FROM Topic1.view_Jobs vjb
ORDER BY 2 ASC, 1 ASC;
GO 

DROP VIEW Topic1.view_JOBs;
GO

--Q4 Add Encryption 
CREATE VIEW Topic1.view_Jobs ([Job], [Department])
WITH ENCRYPTION -- Encrypts the SELECT statment below
AS SELECT jb.[Job Title],
	   dep.[Department Name]
	FROM [Topic1].[Job] jb
	 INNER JOIN [Topic1].[Department] dep
	 ON (jb.[Department ID] = dep.[Department ID]);
GO

DROP VIEW Topic1.view_Jobs;
GO

--Q5 Add schema binding and metadata 
CREATE VIEW Topic1.view_Jobs([Job], [Department])
WITH SCHEMABINDING, VIEW_METADATA
AS SELECT [Job Title],
		  [Department Name]
	FROM Topic1.Job job 
		 INNER JOIN [Topic1].Department dep
		 ON (job.[Department ID] = dep.[Department ID]);
GO

DROP VIEW Topic1.view_Jobs;
GO

--Q6  -Use the previous view to insert a new job 
-- named  IT Clerk in department IT 

INSERT INTO Topic1.view_Jobs ([Job], [Department])
VALUES ('IT Clerk', 'IT');
GO
-- This INSERT only works if you have a view based on 1 (one) table ONLY

SELECT * FROM Topic1.view_Jobs;
GO

--Q7 Create a view named view_empdetails that must contain the 
-- coloumns first name, last name and job id
CREATE VIEW Topic1.view_empdetails ([Name], [Surname], [Job ID])
AS SELECT emp.[First Name],
		  emp.[Last Name],
		  emp.[Job ID]
   FROM  [Topic1].[Employee] emp;
GO

SELECT * FROM Topic1.view_empdetails;

--Q8 Use the view to insert a new employee Tony Zammit working as a Clerk
INSERT INTO Topic1.view_empdetails
VALUES ('Tony', 'Zammit', 
(SELECT job.[Job ID]
FROM  Topic1.Job job
WHERE job.[Job Title] = 'Clerk') 
);
GO

SELECT * FROM  Topic1.view_empdetails;

--Q9 - Show the first name, last name, and job id 
-- of all the employees that are in the IT department

SELECT emp.[First Name],
	   emp.[Last Name],
	   job.[Job ID]
FROM Topic1.Employee emp
	 JOIN Topic1.Job job
	 ON (emp.[Job ID] = job.[Job ID])
	 JOIN Topic1.Department dep 
	 ON (job.[Department ID] = dep.[Department ID])
WHERE dep.[Department Name] = 'IT';

GO

--Q10 - place the previous into a view

CREATE VIEW Topic1.view_ITEmps ([Name], [Surname], [Job Id])
AS (
	SELECT emp.[First Name],
		   emp.[Last Name],
		   emp.[Job ID]
	FROM Topic1.Employee emp
	 JOIN Topic1.Job job
	 ON (emp.[Job ID] = job.[Job ID])
	 JOIN Topic1.Department dep 
	 ON (job.[Department ID] = dep.[Department ID])
WHERE dep.[Department Name] = 'IT'
   );
GO

SELECT * FROM Topic1.view_ITEmps;
GO


--Q11 Insert in the view (Q9) Sarah Tanti as an HR Manager
INSERT INTO Topic1.view_ITEmps([Name],[Surname], [Job Id])
VALUES ('Sarah', 'Tanti', 
(SELECT jb.[Job ID] FROM Topic1.Job jb WHERE [Job Title] = 'HR Manager'));
GO

--Q12 Change the view view_ITEmps so that only rows visible by this view can
-- be inserted, updated or deleted
ALTER VIEW Topic1.view_ITEmps([Name], [Surname], [Job Id])
AS 
    SELECT emp.[First Name],
		   emp.[Last Name],
		   emp.[Job ID]
	FROM Topic1.Employee emp
	 JOIN Topic1.Job job
	 ON (emp.[Job ID] = job.[Job ID])
	 JOIN Topic1.Department dep 
	 ON (job.[Department ID] = dep.[Department ID])
	WHERE dep.[Department Name] = 'IT'
	WITH CHECK OPTION
GO

--INSERT INTO Topic1.view_ITEmps ([Name], [Surname], [Job Id])
--VALUES

--------------------------------------------------------------
						---SEQUENCES--- 
--------------------------------------------------------------

/*
CREATE SEQUENCE [schema_name . ] sequence_name  
    [ AS [ built_in_integer_type | user-defined_integer_type ] ]  
    [ START WITH <constant> ]  
    [ INCREMENT BY <constant> ]  
    [ { MINVALUE [ <constant> ] } | { NO MINVALUE } ]  
    [ { MAXVALUE [ <constant> ] } | { NO MAXVALUE } ]  
    [ CYCLE | { NO CYCLE } ]  
    [ { CACHE [ <constant> ] } | { NO CACHE } ]  
    [ ; ]  
*/

--Q1
Create Schema topic1_1;
GO
Create table topic1_1.Products 
(
	[Pid] INTEGER PRIMARY KEY,
	[Product] VARCHAR (200) NOT NULL,
	PRICE NUMERIC (4,2) NOT NULL
);
GO
--Q2 Create a sequence that starts from 10, increments by 5, hast the smallest value of 10
-- highest value of 15000 and cyles first 20 values must be caches

CREATE SEQUENCE Topic1_1.seq_prod
	START WITH 10
	INCREMENT BY 5
	MINVALUE 10
	MAXVALUE 15000
	CYCLE
	CACHE 20;
GO 

--NOTE: Sequences can be found in [DATABASE NAME]-->Programmiblity --> Sequences

--Q3 Use sys.sequences to check details about your sequence
SELECT * FROM sys.sequences sy
WHERE sy.name = 'seq_prod';

--Q4 Alter Sequence INCREMENT TO 10 

ALTER SEQUENCE Topic1_1.seq_prod
	INCREMENT BY 10;

--Q5 Insert a new product Twistees costing 75c

INSERT INTO topic1_1.Products (Pid, Product, PRICE)
VALUES ((NEXT VALUE FOR Topic1_1.seq_prod), 'Twistees', 0.75);

--Q6 Get current value of the sequence 
SELECT current_value
FROM sys.sequences
WHERE NAME = 'seq_prod';

--Q7 Remove sequence
DROP SEQUENCE Topic1_1.seq_prod;