/*
	IICT6203 - Database Programming II
	Lesson - Views and Sequences (Database Structure)
*/

USE master;
GO

--cleanup
DROP DATABASE IICT6203;
GO

--creation
CREATE DATABASE IICT6203;
GO

USE IICT6203;
GO

CREATE SCHEMA Topic1;
GO

CREATE TABLE Topic1.Department
(
	[Department ID] SMALLINT PRIMARY KEY IDENTITY(1,1),
	[Department Name] NVARCHAR(45) NOT NULL UNIQUE
)
GO

CREATE TABLE Topic1.Job
(
	[Job ID] SMALLINT PRIMARY KEY IDENTITY(1,1),
	[Job Title] NVARCHAR(45) NOT NULL UNIQUE,
	[Department ID] SMALLINT NOT NULL REFERENCES Topic1.Department([Department ID])
)
GO

CREATE TABLE Topic1.Employee
(
	[Employee ID] INTEGER PRIMARY KEY IDENTITY(1,1),
	[First Name] NVARCHAR(45) NOT NULL,
	[Last Name] NVARCHAR(45) NOT NULL,
	Gender CHAR(1),
	[Job ID] SMALLINT NOT NULL REFERENCES Topic1.Job([Job ID])
)
GO

