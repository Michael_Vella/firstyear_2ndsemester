/*
	IICT6203 - Database Programming II
	Lesson - Views and Sequences (Data Population)
*/

USE IICT6203;
GO

INSERT INTO Topic1.Department ([Department Name])
VALUES	('IT'), ('HR'), ('R&D');


INSERT INTO  Topic1.Job ([Job Title], [Department ID])
VALUES ('Developer', (SELECT [Department ID] 
                      FROM Topic1.Department
					  WHERE [Department Name]='IT')),
		('Network Administrator', (SELECT [Department ID] 
                                  FROM Topic1.Department
					              WHERE [Department Name]='IT')),
		('Clerk', (SELECT [Department ID] 
                   FROM Topic1.Department
				   WHERE [Department Name] = 'HR')),
		('IT Manager', (SELECT [Department ID] 
                        FROM Topic1.Department
					    WHERE [Department Name]='IT')),
		('HR Manager', (SELECT [Department ID] 
                        FROM Topic1.Department
					    WHERE [Department Name]='HR'));


INSERT INTO Topic1.Employee ([First Name], [Last Name], [Gender], [Job ID])
VALUES ('Joe', 'Borg', 'M', (SELECT [Job ID] 
                             FROM Topic1.Job
							 WHERE [Job Title] = 'IT Manager')),
	   ('Lisa', 'Abela', 'F', (SELECT [Job ID] 
                               FROM Topic1.Job
							    WHERE [Job Title] ='Developer'));
GO